# PassMaker v1.3
> Google Chrome extension - the best password generator.

https://chrome.google.com/webstore/detail/passmaker-is-the-best-pas/jidfoanpehhinoikkbfklnllbeppgfmi

## History:
### v1.3 _(05/08/2020)_
* Reworked user interface
* Optimization and speed up improved
### v 1.2 _(11/15/2019)_
* Added multi-language support
### v 1.1 _(5/15/2019)_
* Added possibility to exclude chars from generated password