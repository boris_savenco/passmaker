import { LenRange } from './lenrange';
import { Controller } from './controller';
import { Settings } from './settings';
import { Locales } from './locales';

document.addEventListener('DOMContentLoaded', () => {
    const settings = new Settings(()=>{
        new Controller(settings);
        new LenRange('#rangePwdSize');
        new Locales();
    });
});
