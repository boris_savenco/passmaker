export class LenRange {
    private MAX_VAL_DANGER = 9;
    private MAX_VAL_WARNING = 13;

    private CSS_CLASS_DANGER = 'lenrange__bubble_danger';
    private CSS_CLASS_WARNING = 'lenrange__bubble_warning';

    private elInput: HTMLInputElement;
    private elLabel: HTMLLabelElement;
    private elBuble: HTMLElement;

    constructor(wrapperSelector: string){
        this.elLabel = document.querySelector(wrapperSelector) as HTMLLabelElement; 
        this.elInput = this.elLabel.querySelector('input[type="range"]') as HTMLInputElement;
        this.elBuble = this.elLabel.querySelector('.lenrange__bubble') as HTMLElement;
    
        this.elInput.addEventListener('input', this.updateBubble.bind(this));
        this.elInput.addEventListener('change', this.updateBubble.bind(this));
        this.updateBubble();
    }

    updateBubble(){
        const min = +this.elInput.min || 0;
        const max = +this.elInput.max || 100;
        const value = +this.elInput.value || min;

        const offset = (value - min) * 100 / (max - min);

        this.elBuble.textContent = `${value}`;
        this.elBuble.style.left = `${offset}%`;

        this.elBuble.classList.toggle(this.CSS_CLASS_DANGER, value <= this.MAX_VAL_DANGER);
        this.elBuble.classList.toggle(this.CSS_CLASS_WARNING, value <= this.MAX_VAL_WARNING && value > this.MAX_VAL_DANGER);

    }
}