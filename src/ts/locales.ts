type LocElementType = 'title' | 'innerText';
interface LocElement {
    type: LocElementType;
    key: string;
}

export class Locales{
    translateArr: LocElement[] = [
        {type: 'title', key: 'btn_copy_to_clipboard'},
        {type: 'title', key: 'btn_new_password'},
        {type: 'innerText', key: 'pop_copied'},
        {type: 'innerText', key: 'lbl_size'},
        {type: 'innerText', key: 'lbl_include'},
        {type: 'innerText', key: 'lbl_letters'},
        {type: 'innerText', key: 'lbl_numbers'},
        {type: 'innerText', key: 'lbl_symbols'},
        {type: 'innerText', key: 'lbl_extra_symbols'},
        {type: 'innerText', key: 'lbl_exclude'},
        {type: 'innerText', key: 'btn_reset'},
        {type: 'innerText', key: 'btn_settings_hide'},
        {type: 'innerText', key: 'btn_settings_show'},
        {type: 'innerText', key: 'p_privacy'},
      ]

    constructor(){
        this.translateArr.forEach( (lEl: LocElement) => {
            const domElement = document.getElementsByClassName(`js-i18n-${lEl.key}`)[0] as HTMLElement;
            if(!domElement){
                console.warn(lEl.key);
            }
            const translation = (window as any).chrome.i18n.getMessage(lEl.key);
            if (lEl.type === 'innerText'){
                domElement.innerText = translation;
            } else {
                domElement[lEl.type] = translation;
            }
        })
    }
}