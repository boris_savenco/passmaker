interface PwdSettings {
    length: number;
    addNumbers: boolean; 
    addSymbols: boolean;
    addExtraSymbols: boolean;
    exclude: null | string
}
export class PasswordGenerator{  
    static getNewPassword(settings: PwdSettings): string{
        let {length, addNumbers, addSymbols, addExtraSymbols, exclude} = settings;
        const letLC = 'qwertyuiopasdfghjklzxcvbnm';
        let dictionary: string[] = [letLC, letLC.toUpperCase()];

        addNumbers && dictionary.push('1234567890');
        addSymbols && dictionary.push('!#$()+/:<=>?@[]^_{|}');
        addExtraSymbols && dictionary.push('\'\\`";&~,-.*%');
        
        if (typeof exclude === 'string'){
            const ds = ['-', '[', ']', '(', ')', '^'];
            exclude = ds.reduce((str, symbol) => { return str.replace(symbol, `\\${symbol}`) }, exclude);
            const rexExclude = new RegExp('[' + exclude + ']','g');
            dictionary = dictionary.map( page => page.replace(rexExclude, ''));
        }

        dictionary = dictionary.sort( () => PasswordGenerator.chooseRnd(1, -1) );

        let newPassword = '';
        const eachPageLength = (length / dictionary.length) | 0;
        let extraLength = length - (eachPageLength * dictionary.length);
        for (const page of dictionary){
            newPassword += PasswordGenerator.randomizeString(page).substr(0, eachPageLength + extraLength);

            extraLength = 0;
        }

        return PasswordGenerator.randomizeString(newPassword);
    }

    static randomizeString(str: string){
        return str.split('').sort(() => PasswordGenerator.chooseRnd(1, -1)).join('');
    }

    static chooseRnd(a: number, b: number){
        const iter = Date.now() % 10 + 1;
        let val = 0;

        for(let t = 0; t < iter; t++){
          val += Math.random();
        }

        if (Date.now() % 100 > 50) {
          return val / iter >= 0.5 ? b : a;
        } else {
          return val / iter >= 0.5 ? a : b;
        }
    }
}