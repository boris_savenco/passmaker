import { Settings } from "./settings";
import { PasswordGenerator } from "./password-generator";

interface AnimationFrame{
    action(): void;
    delay: number;
}

export class Controller{
    passwordValue = document.getElementById('passwordValue') as HTMLInputElement;
    buttonNewPwd = document.getElementById('buttonNewPwd') as HTMLButtonElement;
    buttonCopyToClipboard = document.getElementById('buttonCopyToClipboard') as HTMLButtonElement;
    
    settingsContainer = document.getElementById('settingsContainer') as HTMLDivElement;
    pwdSizeInput = document.getElementById('pwdSizeInput') as HTMLInputElement;
    pwdChckNumbers = document.getElementById('pwdChckNumbers') as HTMLInputElement;
    pwdChckSymbols = document.getElementById('pwdChckSymbols') as HTMLInputElement;
    pwdChckExtraSymbols = document.getElementById('pwdChckExtraSymbols') as HTMLInputElement;
    
    switcherExclude = document.getElementById('switcherExclude') as HTMLInputElement;
    pwdExclude = document.getElementById('pwdExclude') as HTMLInputElement;
    buttonExcludeReset = document.getElementById('buttonExcludeReset') as HTMLButtonElement;

    buttonSwitchSettings = document.getElementById('switchSettings') as HTMLButtonElement;
    
    popoverCopied = document.getElementById('popoverCopied') as HTMLDivElement;
    
    constructor(private settings: Settings){
        this.defineEvents();
        this.refreshView();
    }

    defineEvents(){
        
        this.buttonNewPwd.addEventListener('click', () => {
            this.getNewPassword(false);
        });
        
        this.buttonExcludeReset.addEventListener('click', () => {
            this.settings.resetPwdExcludeSymbols();
            this.pwdExclude.value = '' + this.settings.get('pwdExclude');
            this.getNewPassword(false);
        });

        this.pwdChckNumbers.addEventListener('change', () => {
            this.settings.set('chkNumbers', this.pwdChckNumbers.checked);
            this.getNewPassword(false);
        });

        this.pwdChckSymbols.addEventListener('change', () => {
            this.settings.set('chkSymbols', this.pwdChckSymbols.checked);
            this.getNewPassword(false);
        });

        this.pwdChckExtraSymbols.addEventListener('change', () => {
            this.settings.set('chkExSymbols', this.pwdChckExtraSymbols.checked);
            this.getNewPassword(false);
        });

        this.switcherExclude.addEventListener('change', () => {
            this.settings.set('pwdExcludeEnabled', this.switcherExclude.checked);
            this.pwdExclude.disabled = !this.settings.get('pwdExcludeEnabled');
            this.buttonExcludeReset.disabled = !this.settings.get('pwdExcludeEnabled');
            this.getNewPassword(false);
        });

        this.buttonCopyToClipboard.addEventListener('click', () => {
            this.passwordValue.select();
            document.execCommand('copy');
            this.popupTextCopied();
        });

        this.buttonSwitchSettings.addEventListener('click', () => {
            const newValue = !this.settings.get('showSettings');
            this.settings.set('showSettings', newValue);
            this.buttonSwitchSettings.classList.toggle('settings-switcher__button_showed', newValue);
            this.settingsContainer.classList.toggle('settings-container_show', newValue);
        });

        this.pwdSizeInput.addEventListener('input', () => {
            this.settings.set('pwdSize', this.pwdSizeInput.value);
            this.refreshPasswordInputFontSize();
            this.getNewPassword(false);     
        });

        this.pwdExclude.addEventListener('change', () => {
            const newValue = this.pwdExclude.value;  
            const filtredNewValue = newValue.split('').filter((l, p, arr) => arr.indexOf(l) === p).join('');
            if (newValue !== filtredNewValue){
                this.pwdExclude.value = filtredNewValue;
            }
            this.settings.set('pwdExclude', filtredNewValue);
            this.getNewPassword(false);
        });
    }

    refreshView(){
        const showSettings = !!this.settings.get('showSettings');
        this.settingsContainer.classList.toggle('settings-container_show', showSettings);
        this.buttonSwitchSettings.classList.toggle('settings-switcher__button_showed', showSettings);
        this.pwdSizeInput.value = '' + this.settings.get('pwdSize');
        this.pwdChckNumbers.checked = !!this.settings.get('chkNumbers');
        this.pwdChckSymbols.checked = !!this.settings.get('chkSymbols');
        this.pwdChckExtraSymbols.checked = !!this.settings.get('chkExSymbols');
        this.switcherExclude.checked = !!this.settings.get('pwdExcludeEnabled');
        this.pwdExclude.value = '' + this.settings.get('pwdExclude');
        this.pwdExclude.disabled = !this.settings.get('pwdExcludeEnabled');
        this.buttonExcludeReset.disabled = !this.settings.get('pwdExcludeEnabled');
        this.refreshPasswordInputFontSize();
        this.getNewPassword(false);

        document.getElementsByClassName('no-css-transition')[0].classList.remove('no-css-transition');
    }

    refreshPasswordInputFontSize(){
        const sizes: {[T:number]: string} = {
          8: '34px',
          9: '34px',
          10: '34px',
          11: '34px',
          12: '34px',
          13: '34px',
          14: '34px',
          15: '31px',
          16: '29px',
          17: '27px',
          18: '26px',
          19: '24px',
          20: '23px',
          21: '22px',
          22: '21px',
          23: '20px',
          24: '19px',
          25: '19px',
          26: '18px',
          27: '17px',
          28: '16px',
          29: '16px',
          30: '15px',
          31: '15px',
          32: '15px',
        };

        const passwordSize = +this.settings.get('pwdSize');

        this.passwordValue.style.fontSize = sizes[passwordSize]; 
      }

    getNewPassword(noFocus: boolean){
        const excludeSymbols = !!this.settings.get('pwdExcludeEnabled') ? '' +this.settings.get('pwdExclude') : null;
        const newPassword = PasswordGenerator.getNewPassword({
            length: +this.settings.get('pwdSize'),
            addNumbers: !!this.settings.get('chkNumbers'),
            addSymbols: !!this.settings.get('chkSymbols'),
            addExtraSymbols: !!this.settings.get('chkExSymbols'),
            exclude: excludeSymbols,
        });
        
        this.passwordValue.value = newPassword;

        if(!noFocus){
            this.passwordValue.select();
        }
    }

    popupTextCopied(){
        if(this.popoverCopied.style.display === 'none'){
            const popoverCopied = this.popoverCopied;
            const animationChain: AnimationFrame[] = [
                {
                    action() { popoverCopied.style.display = 'block' },
                    delay: 0
                },
                {
                    action() { popoverCopied.classList.add('popover__shown') },
                    delay: 1500
                },
                {
                    action() { popoverCopied.classList.remove('popover__shown')  },
                    delay: 300
                },
                {
                    action() { popoverCopied.style.display = 'none' },
                    delay: 0
                },
            ];
            const doNextFrame = (arrChain: AnimationFrame[]) => {
                if(arrChain.length > 0) {
                    let frame = arrChain.shift();
                    frame!.action();
                    setTimeout(()=>{ doNextFrame(arrChain) }, frame!.delay);
                }
            }
            doNextFrame(animationChain);
        }
    }
}


