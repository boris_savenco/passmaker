type SettingName = 'pwdSize' | 'chkNumbers' | 'chkSymbols' | 'chkExSymbols' | 'pwdExcludeEnabled' | 'pwdExclude' | 'showSettings';
type SettingValue = boolean | string | number;

interface SettingsList {
    [T: string]: SettingValue
}

export class Settings{
    default: SettingsList = {
        pwdSize: 12,
        chkNumbers: true,
        chkSymbols: true,
        chkExSymbols: true,
        pwdExcludeEnabled: true,
        pwdExclude: `oO0Il|1\`'"`,
        showSettings: true
    }

    current: SettingsList;

    updSettings = {};
    updSettingsTimerId:any = 0;

    constructor(private clbAllLoaded: ()=> void){
        this.current = {...this.default};
        chrome.storage.local.get(Object.keys(this.default), (result: SettingsList) => {
            this.current = {...this.current, ...result};
            this.clbAllLoaded();
        });
    }
    
    get(settingName: SettingName): SettingValue{
        return this.current[settingName];
    }
    
    set(settingName: SettingName, newValue: SettingValue){
        this.current[settingName] = newValue;
        const newSetting = { [settingName]: newValue };
        this.updSettings = { ...this.updSettings, ...newSetting };
        clearTimeout(this.updSettingsTimerId);
        
        this.updSettingsTimerId = setTimeout(() => {
            chrome.storage.local.set(this.updSettings);
            this.updSettings = {};
        }, 200);
    }

    resetPwdExcludeSymbols(){
        this.set('pwdExclude', this.default.pwdExclude);
    }
}